<?php

namespace App\Livewire;

use Livewire\Component;
use App\Models\Customer;

class CreateCustomer extends Component
{
    public $name;
    public $email;
    public $phone;
    public $address;
    public $gender;
    public $birthdate;

    protected $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|unique:customers,email',
        'phone' => 'required|string|max:20',
        'address' => 'required|string|max:255',
        'gender' => 'required|in:masculino,femenino,otro',
        'birthdate' => 'required|date',
    ];

    public function render()
    {
        return view('livewire.create-customer');
    }

    public function save()
    {
        $this->validate();


        Customer::create([
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'gender' => $this->gender,
            'birthdate' => $this->birthdate,
        ]);


        session()->flash('success', 'Cliente creado exitosamente.');


        $this->reset();

        return redirect()->to('/customers');
    }
}
