<?php

namespace App\Livewire;

use App\Models\Customer;
use Livewire\Component;

class Customers extends Component
{
    public $customers = [];

    public function mount()
    {
        $this->loadCustomers();
    }

    public function delete($customerId)
    {
        Customer::findOrFail($customerId)->delete();
        session()->flash('success', 'Cliente eliminado');
        $this->loadCustomers();
    }

    public function loadCustomers()
    {
        $this->customers = Customer::all();
    }

    public function render()
    {
        return view('livewire.customers');
    }
}
