<head>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>
<div class="card offset-3 col-6">
    <div class="card-header">
        Crea Cliente
    </div>
    <div class="card-body">
        <form action="{{ route('customers.update', $customer) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="mb-3">
                <label for="name" class="form-label">Nombre</label>
                <input id="name" name="name" type="text" class="form-control" value="{{ $customer->name }}" aria-describedby="nameHelp">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Dirección de Correo Electrónico</label>
                <input id="email" name="email" type="email" class="form-control" value="{{ $customer->email }}" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label for="phone" class="form-label">Teléfono</label>
                <input id="phone" name="phone" type="text" class="form-control" value="{{ $customer->phone }}">
            </div>
            <div class="mb-3">
                <label for="address" class="form-label">Dirección:</label>
                <input id="address" name="address" type="text" class="form-control" value="{{ $customer->address }}">
            </div>

            <div class="mb-3">
                <label for="gender" class="form-label">Género:</label>
                <select id="gender" name="gender" class="form-select">
                    <option value="masculino" @if($customer->gender == 'masculino') selected @endif>Masculino</option>
                    <option value="femenino" @if($customer->gender == 'femenino') selected @endif>Femenino</option>
                    <option value="otro" @if($customer->gender == 'otro') selected @endif>Otro</option>
                </select>
            </div>

            <div class="mb-3">
                <label for="birthdate" class="form-label">Fecha de Nacimiento:</label>
                <input id="birthdate" name="birthdate" type="date" class="form-control" value="{{ $customer->birthdate }}">
            </div>

            <button type="submit" class="btn btn-primary">Guardar cambios</button>
        </form>
    </div>
</div>
