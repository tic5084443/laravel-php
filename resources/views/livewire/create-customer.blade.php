<div>
    <div class="card offset-3 col-6">
        <div class="card-header">
          Crea Cliente
        </div>
        <div class="card-body">
          
            <form wire:submit="save">
                <div class="mb-3">
                  <label class="form-label">Nombre</label>
                  <input wire:model="name" type="text" class="form-control" aria-describedby="nameHelp">
                  @error('name')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>
                <div class="mb-3">
                    <label class="form-label">Dirección de Correo Electrónico</label>
                    <input wire:model="email" type="text" class="form-control" aria-describedby="emailHelp">
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="mb-3">
                  <label class="form-label">Teléfono</label>
                  <input wire:model="phone" type="text" class="form-control" >
                  @error('phone')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>
                <div class="mb-3">
                  <label for="address" class="form-label">Dirección:</label>
                  <input type="text" id="address" class="form-control" wire:model="address">
                  @error('address')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>

                <div class="mb-3">
                  <label for="gender" class="form-label">Género:</label>
                  <select id="gender" class="form-select" wire:model="gender">
                    <option value="masculino">Masculino</option>
                    <option value="femenino">Femenino</option>
                    <option value="otro">Otro</option>
                  </select>
                  @error('gender')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>

                <div class="mb-3">
                  <label for="birthdate" class="form-label">Fecha de Nacimiento:</label>
                  <input type="date" id="birthdate" class="form-control" wire:model="birthdate">
                  @error('birthdate')
                      <span class="text-danger">{{ $message }}</span>
                  @enderror
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
              </form>

        </div>
      </div>
</div>
