<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<div class="card offset-3 col-6">
    <div class="card-header">
        Detalles del Cliente
    </div>
    <div class="card-body">
        <div class="mb-3">
            <label for="name" class="form-label">Nombre</label>
            <input id="name" name="name" type="text" class="form-control" value="{{ $customer->name }}" readonly>
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Dirección de Correo Electrónico</label>
            <input id="email" name="email" type="email" class="form-control" value="{{ $customer->email }}" readonly>
        </div>
        <div class="mb-3">
            <label for="phone" class="form-label">Teléfono</label>
            <input id="phone" name="phone" type="text" class="form-control" value="{{ $customer->phone }}" readonly>
        </div>
        <div class="mb-3">
            <label for="address" class="form-label">Dirección:</label>
            <input id="address" name="address" type="text" class="form-control" value="{{ $customer->address }}" readonly>
        </div>
        <div class="mb-3">
            <label for="gender" class="form-label">Género:</label>
            <input id="gender" name="gender" type="text" class="form-control" value="{{ $customer->gender }}" readonly>
        </div>
        <div class="mb-3">
            <label for="birthdate" class="form-label">Fecha de Nacimiento:</label>
            <input id="birthdate" name="birthdate" type="text" class="form-control" value="{{ $customer->birthdate }}" readonly>
        </div>
        <a class="btn btn-primary">Volver</a>
    </div>
</div>
